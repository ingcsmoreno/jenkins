# Jenkins Flavors #

Hi There, as may already noticed, I'm not a Jenkins developer. What you will find here are Jenkins images prepared to run build with specific tools like **LaTeX**.

But, why prepare a full Jenkins instead of using a node??? Well Timmy, since this is a Docker image, you may only want to build a single project, and don't have
the time to set up a full Multi-nodes Jenkins.
