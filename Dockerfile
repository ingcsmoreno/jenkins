FROM jenkins

USER root

RUN apt-get update && apt-get install -y texlive-full \
  && apt-get autoclean && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

USER jenkins
